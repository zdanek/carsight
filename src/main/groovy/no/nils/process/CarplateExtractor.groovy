package no.nils.process;

import com.googlecode.javacv.cpp.opencv_core
import com.googlecode.javacv.cpp.opencv_core.CvMemStorage;

/**
 * User: nilslarsgard
 * Date: 9/5/12
 * Time: 9:20 PM
 */
public interface CarplateExtractor {

    opencv_core.IplImage extract(opencv_core.IplImage imageToProcess)

}
