package no.nils.process

import com.googlecode.javacpp.Loader
import com.googlecode.javacv.cpp.opencv_core
import groovy.transform.CompileStatic

import static com.googlecode.javacv.cpp.opencv_core.*
import static com.googlecode.javacv.cpp.opencv_imgproc.*

/**
 * User: nilslarsgard
 * Date: 9/5/12
 * Time: 9:21 PM
 */
public class CarplateExtractorImpl implements CarplateExtractor {

    int acceptedSkew = 5
    double acceptedRatioDelta = 0.25
    double wantedWidthHeightRatio = 4

    @Override
    public opencv_core.IplImage extract(opencv_core.IplImage imageToProcess) {

        IplImage reSizedImage = resizeImage(imageToProcess)

        IplImage grayScaleImage = toGrayScale(reSizedImage)

        IplImage edgesFromImage = detectEdges(grayScaleImage)

        erodeAndDilate(edgesFromImage)

        CvMemStorage storage = CvMemStorage.create();
        CvSeq contour = new CvSeq();
        cvFindContours(edgesFromImage, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

        Collection<IplImage> images
        images = extractAreasOfInterest(contour, storage, reSizedImage)

        return reSizedImage
    }

    private Collection<IplImage> extractAreasOfInterest(CvSeq contour, CvMemStorage storage, IplImage reSizedImage) {
        Collection<IplImage> detectedRectangles = new ArrayList<IplImage>();
        while (contour && !contour.isNull()) {
            if (contour.elem_size() > 0) {

                CvSeq points = cvApproxPoly(contour, Loader.sizeof(CvContour.class),
                        storage, CV_POLY_APPROX_DP, cvContourPerimeter(contour) * 0.02, 0);
                if (isRectangle(points)) {
                    cvDrawContours(reSizedImage, points, CvScalar.RED, CvScalar.RED, -1, 5, CV_AA);
                    detectedRectangles.add( extractRectangleToImage(points, reSizedImage) )
                }
            }
            contour = contour.h_next();
        }
        return detectedRectangles
    }

    private IplImage extractRectangleToImage(CvSeq points, IplImage reSizedImage) {
        def rectanglePoints = extractPointsFromSequence(points);
        int x = rectanglePoints.sort { it.x() }.get(0).x()
        int y = rectanglePoints.sort { it.y() }.get(0).y()
        int width = rectanglePoints.sort { -it.x() }.get(0).x() - x
        int height = rectanglePoints.sort { -it.y() }.get(0).y() - y
        CvRect rectangle = new CvRect(x,y,width, height)
        cvSetImageROI(reSizedImage, rectangle)
        createImage(width, height)
//        return subImage
    }

    boolean isRectangle(CvSeq points) {
        if (points.total() != 4) {
            return false
        }
        List<CvPoint> pointList = extractPointsFromSequence(points)

        pointList.sort { it.x() }

        int leftWallSkew = Math.abs(pointList[0].x() - pointList[1].x())
        int rightWallSkew = Math.abs(pointList[2].x() - pointList[3].x())
        int width = Math.abs(pointList[0].x() - pointList[2].x())

        pointList.sort { it.y() }

        int topLineSkew = Math.abs(pointList[0].y() - pointList[1].y())
        int bottomLineSkew = Math.abs(pointList[2].y() - pointList[3].y())
        int height = Math.abs(pointList[0].y() - pointList[2].y())

        boolean isRectangle = (Math.abs(leftWallSkew - rightWallSkew) < acceptedSkew) && (Math.abs(topLineSkew - bottomLineSkew) < acceptedSkew)

        if (!isRectangle) {
            return false
        }

        boolean isRatioCorrect = false;

        try { isRatioCorrect = (width / height) > (wantedWidthHeightRatio - acceptedRatioDelta) && (width / height) < (wantedWidthHeightRatio + acceptedRatioDelta) } catch (ArithmeticException e) {}

        return isRectangle && isRatioCorrect
    }

    @CompileStatic
    private List<CvPoint> extractPointsFromSequence(CvSeq points) {
        List<CvPoint> pointList = new ArrayList<CvPoint>();
        for (int i = 0; i < points.total(); i++) {
            CvPoint cvPoint = new CvPoint(cvGetSeqElem(points, i));
            pointList.add(cvPoint);
        }
        pointList
    }

    @CompileStatic
    private void erodeAndDilate(IplImage canny) {
        cvDilate(canny, canny, null, 1)
        cvErode(canny, canny, null, 1)
    }

    @CompileStatic
    private IplImage detectEdges(IplImage gray) {
        IplImage canny = createImage(gray.width(), gray.height())
        cvCanny(gray, canny, 50, 200, 3)
        canny
    }

    @CompileStatic
    private IplImage toGrayScale(IplImage reSizedImage) {
        IplImage gray = createImage(reSizedImage.width(), reSizedImage.height())
        cvCvtColor(reSizedImage, gray, CV_BGR2GRAY)
        gray
    }

    @CompileStatic
    private IplImage resizeImage(IplImage imageToProcess) {
        IplImage reSizedImage = IplImage.create((imageToProcess.width() / 2).intValue(), (imageToProcess.height() / 2).intValue(), imageToProcess.depth(), imageToProcess.nChannels())
        cvResize(imageToProcess, reSizedImage, CV_INTER_LINEAR)
        reSizedImage
    }

    @CompileStatic
    private opencv_core.IplImage createImage(int width, int height) {
        opencv_core.IplImage.create(width, height, IPL_DEPTH_8U, 1)
    }
}
