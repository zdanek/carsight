package no.nils.capture


import com.googlecode.javacv.OpenCVFrameGrabber
import com.googlecode.javacv.cpp.opencv_core

/**
 * User: nilslarsgard
 * Date: 9/2/12
 * Time: 7:26 PM
 */
public class ImageCapturer {
    private OpenCVFrameGrabber grabber;

    ImageCapturer() {
        grabber = new OpenCVFrameGrabber(0);
        grabber.start();
    }

    opencv_core.IplImage capture() {
        grabber.grab()
    }

    void shutDown() {
        grabber.stop()
    }

}
