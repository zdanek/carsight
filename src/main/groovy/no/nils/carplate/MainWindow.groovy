package no.nils.carplate

import com.googlecode.javacv.CanvasFrame
import no.nils.process.CarplateExtractorImpl
import no.nils.capture.ImageCapturer
import no.nils.process.CarplateExtractor

import static com.googlecode.javacv.cpp.opencv_core.IplImage
import groovy.transform.CompileStatic

/**
 * User: nilslarsgard
 * Date: 9/5/12
 * Time: 7:30 PM
 */
public class MainWindow {

    private CanvasFrame canvasFrame
    private ImageCapturer imageCapturer
    private CarplateExtractor carplateExtractor

    public MainWindow() {
        canvasFrame = new CanvasFrame("Web Cam")
        canvasFrame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE)
        imageCapturer = new ImageCapturer()
        carplateExtractor = new CarplateExtractorImpl()
    }


    @CompileStatic
    private void runMainLoop() {
        new Thread() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                long numberOfFrames = 0;
                while (true) {
                    IplImage processedImage = carplateExtractor.extract(imageCapturer.capture())
                    canvasFrame.showImage(processedImage)
                    if (++numberOfFrames % 100 == 0) {
                        System.out.println("FPS:\t" + (numberOfFrames * 1000.0) / (System.currentTimeMillis() - start))
                        numberOfFrames = 0;
                        start = System.currentTimeMillis()
                    }
                }
            }
        }.start()
    }

    public static void main(String[] args) {

        new MainWindow().runMainLoop()
    }

}
