package no.nils.capture;

import com.googlecode.javacv.cpp.opencv_core;
import no.nils.process.ImageCapturer;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * User: nilslarsgard
 * Date: 9/2/12
 * Time: 7:31 PM
 */
public class ImageCapturerTest {

    @Test
    public void capture() {
        ImageCapturer capturer = new ImageCapturer();
        opencv_core.IplImage image = capturer.capture();
        assertThat(image, is(notNullValue()));
    }
}
